import './App.css';
import FormikEx from './mycomponent/FormikEx';

function App() {
  return (
    <div className="App bg-green-300 min-h-screen ">

     <FormikEx/>
    </div>
  );
}

export default App;

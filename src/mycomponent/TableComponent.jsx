import React, { Component } from 'react'
import { useState } from 'react'
import Swal from "sweetalert2";
import "animate.css";


export default class TableComponent extends Component {
constructor() {
  super()

  this.state={ 
      pending:true 
  }
}

changeClick= (item)=> {
    Swal.fire({
      title:   "ID: "+item.id 
       +"\n Email: "+item.email 
       +"\n Username: "+item.username 
       +"\n Age: "+item.age ,


      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
    });
}

  

  render() {
    return (
      
      <div>
        <br /> <br />
        <table class="w-11/12 text-sm  text-gray-500  mx-16 ">
        <thead class="text-xs text-gray-700 uppercase bg-lime-300">
            <tr>
                <th scope="col" class="px-6 py-3">
                    ID
                </th>
                <th scope="col" class=" py-3">
                    EMAIL
                </th>
                <th scope="col" class="px-10 py-3">
                    USERNAME
                </th>
                <th scope="col" class=" py-3">
                    AGE
                </th>
                <th scope="col" class=" py-3">
                    ACTION
                </th>
            </tr>   
        </thead>
        <tbody>
              {this.props.info.map((item, index) => (
              <tr className={index%2 ? 'bg-violet-300': 'bg-white'}  key={item.id} >
                <td className="py-4 text-black ">{item.id} </td>
                <td className="py-4">{item.email == ""? "null": item.email  }</td>
                <td className="px-10 py-4">{item.username ==""? "null":item.username }</td>
                <td className="py-4">{item.age=="" ?"null":item.age }</td>
                <td className="py-4">
                 <button onClick={()=> this.props.pending(item)} className={`px-20 py-3 text-white hover:opacity-60 rounded-md mr-2  ${item.status === "Pending" ? "bg-amber-300"  : "bg-red-400"} ` } > {item.status} </button>
                  <button onClick= {() => this.changeClick(item) } className="bg-blue-600 text-white px-16 py-3 hover:opacity-60 rounded-md">Show more</button>
                </td>
              </tr>
            ))}
          </tbody>
       </table>
      </div>
    )
  }
}

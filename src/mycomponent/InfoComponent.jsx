import React, { Component } from 'react'
import TableComponent from './TableComponent'


export default class InfoComponent extends Component {
    constructor(){
        super()
        this.state = {
           student:[
            {
                id:1,
                email:'khamann@gmail.com',
                username:'Mannz',
                age:20,
                status:"Pending",
              }]   ,
                newUsername:"" ,newEmail:"",newAge:"",newStatus:"Pending"
        }
    }
    // handleUserInput = (haha) => {
    //     const {id,value} = haha.target
    //     this.setState({
    //       [id]: value,
          
    //     });
    //   }

    handleEmailChange = (event) => {
      this.setState({ newEmail: event.target.value });
      
  }

  handleUsernameChange = (event) => {
      this.setState({ newUsername: event.target.value });
  }

  handleAgeChange = (event) => {
      this.setState({ newAge: event.target.value });
  }

     register= ()=> { 
    
        const newStu ={id: this.state.student.length+1, 
                       username: this.state.newUsername,
                       email: this.state.newEmail,
                       age: this.state.newAge ,
                       status:this.state.newStatus,
                        };
                    
          this.setState({
        student: [...this.state.student, newStu],
        newEmail: '',
        newUsername: '',
        newAge: '',
        status:'Pending'
      } 
      );
    };

    handlePendingChange = (event)=>{
        console.log(event)
        this.state.student.map((item)=>{
          if(item.id== event.id){
              item.status == "Pending" ? (item.status="Done" ) : (item.status="Pending")  ;
          }
        })
        this.setState({
            student: this.state.student,
        })
    }

     render(){
    return (
      <div>
        
<label for="email-address-icon" class="block mb-2 text-xl font-bold text-gray-900 text-start mx-16">Your Email</label>
<div class="relative mx-16">
  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
  </div>
                {/* getting email */}
  <input onChange={this.handleEmailChange} value={this.state.newEmail} type="text" id="newEmail" class=" border text-gray-900 text-sm rounded-lg  block w-full pl-10 p-2.5   dark:border-gray-600 " placeholder="name@gmail.com"/>
</div> <br />


<label for="website-admin" className="block mb-2 text-xl font-bold text-gray-900 text-start mx-16">Unsername</label>
<div className="flex mx-16" >
  <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:border-gray-600">
    @
  </span>
   {/* getting username */}
  <input onChange={this.handleUsernameChange} value={this.state.newUsername} type="text" id="newUsername" class="rounded-none rounded-r-lg border border-gray-300 text-gray-900  flex-1 min-w-0 w-full text-sm p-2.5  dark:border-gray-600 dark:placeholder-gray-400" placeholder="example"/>
</div> <br />

<label for="website-admin" class="block mb-2 text-xl font-bold text-gray-900 text-start mx-16">Age</label>
<div class="flex mx-16">
  <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:border-gray-600">
    ❤️
  </span>
   {/*  getting age*/}
  <input onChange={this.handleAgeChange} value={this.state.newAge} type="text" id="newAge" class="rounded-none rounded-r-lg border border-gray-300 text-gray-900  flex-1 min-w-0 w-full  p-2.5  dark:border-gray-600 dark:placeholder-gray-400" placeholder="....."/>
</div> <br />

<button onClick={this.register} class="mt-4 hover:bg-pink-200 bg-white text-black font-bold py-2 px-24 border border-pink-700 rounded">
  Register
</button>  


        {/* for display table */}
            <TableComponent info= {this.state.student} pending={this.handlePendingChange}/>
      </div>
    )
  }
}

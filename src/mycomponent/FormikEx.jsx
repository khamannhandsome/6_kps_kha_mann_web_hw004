import React, { Component } from 'react';
 import { Formik, Form, Field } from 'formik';
 import * as Yup from 'yup';
 const SignupSchema = Yup.object().shape({

    email: Yup.string().email('Invalid email').required('Required'),
    password: Yup.string().matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/,"Password must contain at least 1 uppercase 1 special character 1 numbercdcd").required('Required'),

  });
  export default class FormikEx extends Component {
  render() {
    
    return (
      <div>
        <h1 className='text-3xl'>Sign Up</h1> <br />
     <Formik
       initialValues={{
         email: '',
         password:'',
       }}
       validationSchema={SignupSchema}
       onSubmit={values => {
         // same shape as initial values
         console.log(values);
       }}
     >
       {({ errors, touched }) => (
         <Form>
          <label htmlFor="">Email </label>
          
           <Field name="email" type="email" />
           {errors.email && touched.email ? <div>{errors.email}</div> : null}
           <br /> 
           <label htmlFor="">Passowrd </label>
           <Field name="password" type="password" />
           {errors.password && touched.password ? <div>{errors.password}</div> : null}
           <br /> 
           <button  className='p-2 bg-red-300 rounded-xl' type="submit">Submit</button>
         </Form>
       )}
     </Formik>
      </div>

    )
  }
}
